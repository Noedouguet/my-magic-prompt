#!/bin/bash
source quit.sh
source help.sh
source ls.sh
source about.sh
source version.sh
source age.sh
source pwd.sh
source hour.sh
source passww.sh
source smtp.sh
source open.sh
source error.sh
source httpget.sh
source auth.sh
source profil.sh


#profil : permet d’afficher toutes les informations sur vous même.  First Name, Last name, age, email
#httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le nom du fichier.


cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;; #
    help ) help;; #
    about ) about;; #
    version | --v | vers) version;; #
    age ) age;; #
    pwd ) pwd_;; #
    hour ) hour;; #
    ls ) ls  $2 $3;; #
    rm ) rm  $2 $3;; #
    rmdir | rmd ) rmdir $2 $3;; #
    cd ) cd $2;; #
    smtp ) smtp;;
    passw ) passw;; #
    open ) open $2;; #
    httpget ) httpget $2;; #
    profil ) profil;;
    * ) error;; #
  esac
}

main() {
  auth
  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mNoe\033[m ☠️ : ${pwd_}"
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
