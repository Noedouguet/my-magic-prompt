auth(){
    if [ "$result" = false ] || [ "$result" = "" ]; then
        compte=(
            "Noe 1234 Noe Douguet noedouguet@gmail.com 20"
            "Maman 1234 Maman Noe mamamnoe@gmail.com 45"
            "Alex 1234 Alexandre Pereira alexandrepr@gmail.com 21"
        )
    fi

    echo "Avez-vous un compte ? (O/n)"
    read response

    if [ "$response" = "O" ]; then
        echo "Entrez votre login : "
        read login

        result=false
        index=-1

        for i in "${!compte[@]}"; do
            champs=(${compte[i]})
            if [ "$login" = "${champs[0]}" ]; then
                result=true
                index=$i
                break
            fi
        done

        if [ "$result" = true ]; then
            echo "Entrez votre mot de passe : "
            read -s mdp
 
            if [ "$mdp" = "${champs[1]}" ]; then
                echo "Vous êtes connecté !"
                profil=("Prénom: ${champs[2]}" "Nom: ${champs[3]}" "Mail: ${champs[4]}" "Âge: ${champs[5]}")
            else
                echo "Mauvais mot de passe."
                exit
            fi
        else
            echo "Compte non trouvé."
            exit
        fi
    else
        echo "Création d'un compte."
        echo "Identifiant : "
        read identifiant
        echo "Mot de passe : "
        read -s mdp
        echo "Prénom : "
        read prenom
        echo "Nom : "
        read nom
        echo "Âge : "
        read age
        echo "Adresse mail : "
        read mail
        echo "---------------"
        echo "Votre compte a bien été créé, vous êtes connecté"
        compte=("$identifiant $mdp $prenom $nom $age $mail")
        profil=(
        "Prénom : $prenom "
        "Nom de famille : $nom "
        "Âge : $age "
        "Adresse mail : $mail ")
    fi
}