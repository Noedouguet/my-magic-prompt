
# Documentation du Prompt - my-magic-prompt

Bienvenue dans le prompt interactif my-magic-prompt, un environnement en ligne de commande conçu pour faciliter les tâches courantes et offrir des fonctionnalités supplémentaires utiles. Ce prompt est accessible après avoir fourni un nom d'utilisateur et un mot de passe spécifiques.

## Accès au Prompt

Pour accéder au prompt, exécutez le fichier `main.sh` situé dans le répertoire `~/my-magic-prompt` de votre système. Vous devrez fournir un nom d'utilisateur et un mot de passe spécifiques pour vous connecter.

bashCopy code

`$ ~/my-magic-prompt/main.sh
Entrer votre login :
[Entrez votre login ici]
Entrer votre mot de passe :
[Entrez votre mot de passe ici]` 

Une fois connecté, vous aurez accès à un ensemble de commandes interactives.

## Commandes Disponibles

Le prompt my-magic-prompt offre un ensemble de commandes utiles pour effectuer diverses tâches. Voici la liste des commandes disponibles :

-   **help** : Affiche la liste des commandes disponibles.
-   **ls** : Liste les fichiers et les dossiers, y compris les fichiers cachés.
-   **rm** : Supprime un fichier.
-   **rmd** ou **rmdir** : Supprime un dossier.
-   **about** : Affiche une description du programme.
-   **version**, **--v** ou **vers** : Affiche la version du prompt.
-   **age** : Vous demande votre âge et vous informe si vous êtes majeur ou mineur.
-   **quit** : Permet de quitter le prompt.
-   **profil** : Affiche toutes les informations vous concernant (Prénom, Nom, Âge, Adresse e-mail).
-   **passw** : Permet de changer le mot de passe en demandant une confirmation.
-   **cd** : Vous permet de naviguer dans les dossiers, de créer des dossiers et de revenir en arrière.
-   **pwd** : Indique le répertoire actuel courant.
-   **hour** : Affiche l'heure actuelle.
-   ***** : Indique une commande inconnue.
-   **httpget** : Permet de télécharger le code source HTML d'une page web et de l'enregistrer dans un fichier spécifique. Vous devez spécifier le nom du fichier.
-   **smtp** : Vous permet d'envoyer un e-mail avec une adresse, un sujet et un corps de message spécifiés.
-   **open** : Ouvre un fichier directement dans l'éditeur VIM, même s'il n'existe pas.

## Utilisation

Le prompt my-magic-prompt est conçu pour être interactif et convivial. Vous pouvez entrer des commandes et des arguments directement dans l'invite de commande. Suivez les instructions affichées à l'écran pour interagir avec les différentes commandes.

## Exemples

Voici quelques exemples d'utilisation des commandes disponibles dans le prompt :

-   Pour afficher la liste des commandes disponibles :
    
    bashCopy code
    
    `help` 
    
-   Pour lister les fichiers et les dossiers dans le répertoire actuel, y compris les fichiers cachés :
    
    bashCopy code
    
    `ls` 
    
-   Pour supprimer un fichier nommé `mon_fichier.txt` :
    
    bashCopy code
    
    `rm mon_fichier.txt` 
    
-   Pour changer de répertoire :
    
    bashCopy code
    
    `cd mon_dossier` 
    
-   Pour afficher votre répertoire actuel :
    
    bashCopy code
    
    `pwd` 
    
-   Pour envoyer un e-mail :
    
    bashCopy code
    
    `smtp`