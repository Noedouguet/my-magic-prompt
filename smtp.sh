smtp() {
    # Informations SMTP de Gmail
    SMTP_SERVER="smtp.gmail.com"
    SMTP_PORT="465"
    SMTP_USER="noedouguet@gmail.com"
    SMTP_PASSWORD="ufuq vvfz sbwm xdrc"

    # Demander à l'utilisateur l'adresse e-mail du destinataire
    echo "Entrez l'adresse e-mail du destinataire :"
    read TO

    # Demander à l'utilisateur le sujet de l'e-mail
    echo "Entrez le sujet de l'e-mail :"
    read SUBJECT

    # Demander à l'utilisateur le corps de l'e-mail
    echo "Entrez le corps de l'e-mail (Appuyez sur Ctrl-D pour terminer) :"
    BODY=$(cat)

    # Construire le message SMTP au format MIME
    MESSAGE=$(cat <<EOM
Subject: $SUBJECT
To: $TO
From: $SMTP_USER
Content-Type: text/plain; charset=UTF-8

$BODY
EOM
    )

    # Envoyer l'e-mail en utilisant curl et le serveur SMTP de Gmail
    curl --url "smtps://$SMTP_SERVER:$SMTP_PORT" \
         --ssl-reqd \
         --mail-from "$SMTP_USER" \
         --mail-rcpt "$TO" \
         --user "$SMTP_USER:$SMTP_PASSWORD" \
         --upload-file <(echo -e "$MESSAGE")
}
