age() {
  echo "Veuillez entrer votre âge :"
  read age

  if [ -z "${age}" ]; then
    echo "L'âge n'a pas été spécifié."
  elif [ "${age}" -lt 18 ]; then
    echo "Vous êtes mineur."
  else
    echo "Vous êtes majeur."
  fi
}