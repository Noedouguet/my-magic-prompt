open() {
  # Vérifier si un argument (nom de fichier) a été fourni
  if [ -z "$1" ]; then
    echo "Utilisation : open <nom-du-fichier>"
    return 1
  fi

  file="$1"

  # Vérifier si le fichier existe
  if [ ! -e "$file" ]; then
    # Si le fichier n'existe pas, le créer
    touch "$file"
  fi

  # Ouvrir le fichier dans Vim dans un nouvel onglet
  vim -p "$file"
}