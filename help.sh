help() {
  echo "
    help : commande utilisable.
    ls : liste des fichiers visible/caché.
    rm : supprimer un fichier.
    rmd ou rmdir : supprimer un dossier.
    about : une description du programme.
    version ou --v ou vers :  version du prompt.
    age : saisir votre age et dit si vous etes majeur ou mineur.
    quit : quitter le prompt.
    profil : affiche vos information (First Name, Last name, age, email).
    passw : permet de changer le password avec une demande de confirmation.
    cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier précédent.
    pwd : indique votre répertoire actuelle.
    hour : donne l'heure actuelle.
    * : indiquer une commande inconnu.
    httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique.
    smtp : envoie un mail avec une adresse un sujet et le corp du mail.
    open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas.
    "
}