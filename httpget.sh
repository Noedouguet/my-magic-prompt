httpget() {
  url="$1"  # L'URL est le premier argument passé à la fonction

  echo "Entrez le nom du fichier de sortie :"
  read output_file

  # Utilisation de curl pour télécharger la page web et enregistrer dans le fichier de sortie
  curl -o "$output_file" "$url"

  if [ $? -eq 0 ]; then
    echo "Téléchargement réussi. Le contenu de la page a été enregistré dans '$output_file'."
  else
    echo "Échec du téléchargement de la page web."
  fi
}
